# Cosmos Client Library

This library is intended for Cosmos Publishers only. It helps facilitate the communication between the Cosmos platform and custom widgets.

## Background

The Cosmos platform is designed to be extensible. We understand that are not able to develop every feature or content type for every situation.
However, publishers may want to provide functionality unique to their content solutions. Static content can be included by simply embedding external webpages into Cosmos resources.

Sometimes, static interactions are not enough. You may want to capture and display student responses, or track data as users utilize your custom experience.
The Cosmos Client Library makes it easy to extend Cosmos' capabilities into your own code.

## Getting Started

Download the `cosmos.js` file and add it into your codebase where you include other JavaScript libraries. You can include the library asynchronously as well, just make sure that you do not try to access the functionality before the object is initialized.

The code will execute and provide a global variable called `Cosmos`.

## API Methods

To call methods on the API, all you have to do is call `Cosmos.method(data)`. No setup is required to authenticate the student, etc.

#### Submit Response

```javascript
var data = {
    url: '<<URL>>' //The url for the image that you want to submit on behalf of the student
};

var callback = function(result, error){}; //A optional callback function for when the submission has completed

Cosmos.submit(data, callback);
```
